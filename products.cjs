fetch("https://fakestoreapi.com/products/")
  .then((response) => {
    if (!response.ok) {
      throw Error("Fetch Failed");
    } else {
    return response.json();
    }
  })

  .then((data) => {
    console.log("Data fetch");
    loaderBlocker();
    if (data.length === 0) {
      noProductToShow();
    } else {
      myStore(data);
    }
  })

  .catch((err) => {
    console.error(`error occured on fetching file ${err}`);
    loaderBlocker();
    dispayFailedFetchDocuement();
  });

 function myStore(detail) {
   const unorderList = document.querySelector("ul");

    detail.map((data) => {
    
    const list = document.createElement("li");
    const image = document.createElement("img");
    const heading2 = document.createElement("h2");
    const heading3 = document.createElement("h3");
    const heading4 = document.createElement("h4");
    const button = document.createElement("button");

    image.src = data.image;
    heading2.innerText = data.title;
    heading3.innerText = "₹" + data.price;
    heading4.innerText = "Rating: " + data.rating.rate + "(" + data.rating.count + ")";
    button.innerText = "Buy Now";

    list.append(image, heading2, heading3, heading4, button);
    unorderList.append(list);
  });
}

function dispayFailedFetchDocuement() {
  const errorDisplay = document.querySelector(".failed-fetch");
  errorDisplay.style.display = "block";
}

function loaderBlocker() {
  const loader = document.querySelector(".center");
  loader.remove();
}

function noProductToShow() {
  const failFetch = document.querySelector(".empty-list");
  failFetch.style.display = "block";
}
